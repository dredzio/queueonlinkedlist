package com.epam.ja.Kolejka;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Queue;

public class QueuePollTest {
    @Test
    void pollReturnsFirstElementFromNotEmptyQueue() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1));
        //When
        int firstElement = queue.poll();
        //Then
        Assert.assertEquals(firstElement, 1, "Poll returns first element from non-empty queue");
    }

    @Test(dependsOnGroups = "peekTest")
    void pollRemovesFirstElementFromNotEmptyQueue() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1,2,3));
        //When
        queue.poll();
        int size = queue.size();
        int newHeadElement = queue.peek();
        //Then
        Assert.assertEquals(size, 2, "Poll removes first element from non-empty queue");
        Assert.assertEquals(newHeadElement, 2, "Second element becomes new head");
    }
}
