package com.epam.ja.Kolejka;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Queue;

public class QueueSizeTest {

    @Test(groups = "sizeTest")
    void emptyQueueReturnsSize() {
        //Given
        Queue<Integer> queue = new Kolejka<>();
        //When
        int size = queue.size();
        //Then
        Assert.assertEquals(size, 0, "Empty queue returns size of 0");

    }

    @Test(groups = "sizeTest")
    void nonEmptyQueueReturnsSize() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1,2,3));
        //When
        int size = queue.size();
        //Then
        Assert.assertEquals(size, 3, "Nonempty queue returns size of 0");

    }
}
