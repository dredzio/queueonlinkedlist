package com.epam.ja.Kolejka;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Queue;

public class QueueRemoveTest {

    @Test
    void queueRemovesFirstElement() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1));
        //When
        queue.remove();
        int size = queue.size();
        //Then
        Assert.assertEquals(size, 0, "Remove removes element from non-empty queue");
    }

    @Test(expectedExceptions = NoSuchElementException.class)
    void queueThrowsExceptionWhenQueueIsEmpty() {
        //Given
        Queue<Integer> queue = new Kolejka<>();
        //When
        queue.remove();
        //Then
        //Should throw exception
    }

    @Test
    void removeMethodReturnsRemovedElement() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1));
        //When
        Integer result = queue.remove();
        //Then
        Assert.assertEquals(result, Integer.valueOf(1), "List returns removed element");
    }

    @Test(dependsOnGroups = "peekTest")
    void removeRemovesHeadElement() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1,2,3));
        //When
        Integer removedNumber = queue.remove();
        Integer newHead = queue.peek();
        int size = queue.size();
        //Then
        Assert.assertNotEquals(removedNumber, newHead, "List removes head element");
        Assert.assertEquals(size, 2, "List removes exactly 1 element");
    }
}
