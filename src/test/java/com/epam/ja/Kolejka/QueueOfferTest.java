package com.epam.ja.Kolejka;

import com.epam.ja.ListaLancuchowa.ListaLancuchowa;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Queue;

public class QueueOfferTest {

    @Test
    void offerReturnsTrue() {
        //Given
        Queue<Integer> queue = new Kolejka<>();
        //When
        boolean result = queue.offer(1);
        //Then
        Assert.assertTrue(result, "Offer returns true when element is added");
    }

    @Test(dependsOnGroups = "peekTest")
    void queueOfferAddsElement() {
        //Given
        Queue<Integer> kolejka = new Kolejka<>();
        //When
        kolejka.offer(1);
        int size = kolejka.size();
        //Then
        Assert.assertEquals(size, 1, "Elements are added to linked list");
    }

    @Test(dependsOnGroups = "peekTest")
    void queueOfferAddsElementAtTheEnd() {
        //Given
        Queue<Integer> kolejka = new Kolejka<>();
        //When
        kolejka.offer(1);
        kolejka.offer(2);
        int value = kolejka.peek();
        //Then
        Assert.assertEquals(value, 1, "Elements are added at the end of list");
    }
}
