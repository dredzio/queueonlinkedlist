package com.epam.ja.Kolejka;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Queue;

public class QueuePeekTest {
    @Test(groups = {"peekTest"})
    void peekReturnsFirstElementWhenQueueIsNotEmpty() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(6,2,3));
        //When
        int firstElement = queue.peek();
        //Then
        Assert.assertEquals(6, firstElement, "Queue peek returns first element");
    }

    @Test(groups = {"peekTest"})
    void peekReturnsNullWhenQueueIsEmpty() {
        //Given
        Queue<Integer> queue = new Kolejka<>();
        //When
        Integer firstElement = queue.peek();
        //Then
        Assert.assertNull(firstElement, "Empty queue returns null on peek");
    }

    @Test(groups = {"peekTest"})
    void peekDoesntRemoveElementFromQueue() {
        //Given
        Queue<Integer> queue = new Kolejka<>(Arrays.asList(1,2,3));
        //When
        Integer firstElement = queue.peek();
        int size = queue.size();
        //Then
        Assert.assertEquals(3, size, "Peek doesnt remove element from queue");
    }
}
