package com.epam.ja.ListaLancuchowa;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ListaLancuchowaTest {
    @Test
    void emptyLinkedListReturnsSize() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        int size = lista.size();
        //Then
        Assert.assertEquals(size, 0, "Empty linked list returns size equal to 0");
    }

    @Test(dependsOnMethods = "emptyLinkedListReturnsSize")
    void linkedListAddsElement() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        lista.offer(1);
        int size = lista.size();
        //Then
        Assert.assertEquals(size, 1, "Elements are added to linked list");
    }

    @Test(dependsOnMethods = "emptyLinkedListReturnsSize")
    void linkedListAddsSecondElementAtTheEnd() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        lista.offer(1);
        lista.offer(2);
        int first = lista.peek();
        //Then
        Assert.assertEquals(first, 1, "Elements are added at the end of list, so first element stays 1");
    }

    @Test(dependsOnMethods = "linkedListAddsElement")
    void likedListGetFirstReturnsFirstElement() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        lista.offer(1);
        int value = lista.peek();
        //Then
        Assert.assertEquals(value, 1, "Peek returns first element from list");
    }

    @Test
    void linkedListPollRemovesFirstElement() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        lista.offer(1);
        lista.poll();
        int size = lista.size();
        //Then
        Assert.assertEquals(size, 0, "Poll removes first element from list");
    }

    @Test
    void linkedListPollReturnsFirstElement() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        lista.offer(1);
        int value = lista.poll();
        //Then
        Assert.assertEquals(value, 1, "Poll returns first element from list");
    }

    @Test
    void emptyLinkedListPollReturnsNull() {
        //Given
        ListaLancuchowa<Integer> lista = new ListaLancuchowa<>();
        //When
        Integer value = lista.poll();
        //Then
        Assert.assertNull(value, "Poll returns null from empty list");
    }
}
