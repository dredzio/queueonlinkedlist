package com.epam.ja.Kolejka;

import com.epam.ja.ListaLancuchowa.ListaLancuchowa;

import java.util.*;

public final class Kolejka<T> extends AbstractCollection<T> implements Queue<T> {

    private ListaLancuchowa<T> list = new ListaLancuchowa<>();

    public Kolejka(Collection<T> collection) {
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            list.offer(iterator.next());
        }
    }

    public Kolejka() {

    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean offer(T element) {
        list.offer(element);
        return true;
    }

    @Override
    public T remove() {
        T element = list.poll();
        if(element == null) {
            throw new NoSuchElementException();
        }
        return element;
    }

    @Override
    public T poll() {
        return list.poll();
    }

    @Override
    public T element() {
        return null;
    }

    @Override
    public T peek() {
        return list.peek();
    }
}
