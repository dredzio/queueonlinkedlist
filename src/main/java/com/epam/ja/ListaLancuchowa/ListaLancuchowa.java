package com.epam.ja.ListaLancuchowa;

public final class ListaLancuchowa<T> {
    public ListaLancuchowa() {
        first = null;
        size = 0;
    }

    public T poll() {
        if(first == null) {
            return null;
        }
        T valueOfFirst = first.value;
        removeFirst();
        return valueOfFirst;
    }

    private void removeFirst() {
        first = first.next;
        size --;
    }


    private class Node<T> {
        private Node(T value) {
            this.value = value;
        }

        private Node<T> next;
        private T value;
    }

    private Node<T> first;
    private int size;



    public int size() {
        return size;
    }

    public void offer(T element) {
        insertAtEnd(element);
        size ++;
    }

    public T peek() {
        return first == null ? null : first.value;
    }

    private void insertAtEnd(T element) {
        Node<T> newNode = new Node<>(element);
        if(first == null) {
            this.first = newNode;
        }
        else {
            Node<T> current = first;
            while(current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
    }
}
